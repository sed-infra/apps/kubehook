FROM python

ENV IN_DOCKER=1

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt .

# Install python package
RUN python3 -m pip install --no-cache-dir -r requirements.txt

# Copy the application
COPY . .

CMD [ "python3", "kubehook.py" ]
