import os
import time

from discord_webhook import DiscordEmbed, DiscordWebhook
from kubernetes import client, config, watch


def discordHandler(url, event):
    webhook = DiscordWebhook(url=url)

    if event["type"] == "ADDED":
        color = "32CD32"
    elif event["type"] == "MODIFIED":
        color = "FFFF00"
    elif event["type"] == "DELETED":
        color = "DC143C"
    else:
        color = "C0C0C0"

    embed = DiscordEmbed(
        title="Kubehook",
        description="Event: %s %s %s"  # noqa: MOD001
        % (event["type"], event["object"].kind, event["object"].metadata.name),
        color=color,
    )

    webhook.add_embed(embed)

    webhook.execute()


def main():
    if os.getenv('IN_DOCKER'):
        config.load_incluster_config()
    else:
        config.load_kube_config()

    v1 = client.CoreV1Api()
    w = watch.Watch()  # noqa: VNE001

    init = True
    last_time = time.time()

    for event in w.stream(v1.list_pod_for_all_namespaces, timeout_seconds=0):
        if init:
            if time.time() - last_time < 5:
                continue
            init = False

        print(  # noqa: T001
            "Event: %s %s %s"  # noqa: MOD001
            % (event["type"], event["object"].kind, event["object"].metadata.name)  # noqa: E501
        )

        if os.getenv("DISCORD_HOOK_URL"):
            discordHandler(os.getenv("DISCORD_HOOK_URL"), event)


if __name__ == "__main__":
    main()
